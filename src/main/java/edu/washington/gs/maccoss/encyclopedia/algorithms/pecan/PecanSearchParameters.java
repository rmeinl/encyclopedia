package edu.washington.gs.maccoss.encyclopedia.algorithms.pecan;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import edu.washington.gs.maccoss.encyclopedia.Pecanpie;
import edu.washington.gs.maccoss.encyclopedia.algorithms.percolator.PercolatorExecutor;
import edu.washington.gs.maccoss.encyclopedia.algorithms.percolator.PercolatorVersion;
import edu.washington.gs.maccoss.encyclopedia.algorithms.phospho.PeptideModification;
import edu.washington.gs.maccoss.encyclopedia.algorithms.phospho.ScoringBreadthType;
import edu.washington.gs.maccoss.encyclopedia.datastructures.AminoAcidConstants;
import edu.washington.gs.maccoss.encyclopedia.datastructures.DataAcquisitionType;
import edu.washington.gs.maccoss.encyclopedia.datastructures.SearchParameters;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.DigestionEnzyme;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.FragmentationType;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.MassTolerance;

//@Immutable
public class PecanSearchParameters extends SearchParameters {
	private final int minPeptideLength;
	private final int maxPeptideLength;
	private final int maxMissedCleavages;
	private final byte minCharge;
	private final byte maxCharge;
	private final int numberOfReportedPeaks;
	private final boolean addDecoysToBackgound;
	private final boolean dontRunDecoys; // only for testing
	private final float alpha;
	private final float beta;
	private final boolean requireVariableMods;
	
	public String toString() {
		final StringBuilder sb=new StringBuilder();
		sb.append(" -fixed "+aaConstants.getFixedModString()+"\n");
		sb.append(" -frag "+FragmentationType.toString(fragType)+"\n");
		sb.append(" -ptol "+precursorTolerance.getToleranceThreshold()+"\n");
		sb.append(" -ftol "+fragmentTolerance.getToleranceThreshold()+"\n");
		sb.append(" -ptolunits"+precursorTolerance.getUnits()+"\n");
		sb.append(" -ftolunits"+fragmentTolerance.getUnits()+"\n");
		sb.append(" -poffset "+precursorOffsetPPM+"\n");
		sb.append(" -foffset "+fragmentOffsetPPM+"\n");
		sb.append(" -enzyme "+enzyme.getName()+"\n");
		sb.append(" -minLength "+minPeptideLength+"\n");
		sb.append(" -maxLength "+maxPeptideLength+"\n");
		sb.append(" -maxMissedCleavage "+maxMissedCleavages+"\n");
		sb.append(" -minCharge "+minCharge+"\n");
		sb.append(" -maxCharge "+maxCharge+"\n");
		sb.append(" -expectedPeakWidth "+expectedPeakWidth+"\n");
		sb.append(" -numberOfReportedPeaks "+numberOfReportedPeaks+"\n");
		sb.append(" -addDecoysToBackground "+addDecoysToBackgound+"\n");
		sb.append(" -dontRunDecoys "+dontRunDecoys+"\n");
		sb.append(" -percolatorThreshold "+percolatorThreshold+"\n");
		sb.append(" -percolatorVersionNumber "+percolatorVersionNumber+"\n");
		sb.append(" ").append(OPT_PERC_TRAINING_SIZE).append(" ").append(percolatorTrainingSetSize).append("\n");
		sb.append(" ").append(OPT_PERC_TRAINING_THRESH).append(" ").append(percolatorTrainingSetThreshold).append("\n");
		sb.append(" -acquisition "+DataAcquisitionType.toString(dataAcquisitionType)+"\n");
		sb.append(" -numberOfThreadsUsed "+numberOfThreadsUsed+"\n");
		sb.append(" -numberOfQuantitativePeaks "+numberOfQuantitativePeaks+"\n");
		sb.append(" -minNumOfQuantitativePeaks "+minNumOfQuantitativePeaks+"\n");
		sb.append(" -alpha "+alpha+"\n");
		sb.append(" -beta "+beta+"\n");
		sb.append(" -quantifyAcrossSamples "+quantifyAcrossSamples+"\n");
		sb.append(" -minIntensity "+minIntensity+"\n");
		sb.append(" -requireVariableMods "+requireVariableMods+"\n");
		sb.append(" -precursorIsolationRangeFile "+(precursorIsolationRangeFile.isPresent()?precursorIsolationRangeFile.get().getAbsolutePath():"none")+"\n");
		sb.append(" -percolatorModelFile "+(percolatorModelFile.isPresent()?percolatorModelFile.get().getAbsolutePath():"none")+"\n");
		
		return sb.toString();
	}
	
	public HashMap<String, String> toParameterMap() {
		HashMap<String, String> map=new HashMap<String, String>();
		map.put("-fixed", aaConstants.getFixedModString());
		map.put("-frag", FragmentationType.toString(fragType));
		map.put("-ptol", precursorTolerance.getToleranceThreshold()+"");
		map.put("-ftol", fragmentTolerance.getToleranceThreshold()+"");
		map.put("-ptolunits", precursorTolerance.getUnits());
		map.put("-ftolunits", fragmentTolerance.getUnits());
		map.put("-poffset", precursorOffsetPPM+"");
		map.put("-foffset", fragmentOffsetPPM+"");
		map.put("-enzyme", enzyme.getName());
		map.put("-minLength", minPeptideLength+"");
		map.put("-maxLength", maxPeptideLength+"");
		map.put("-maxMissedCleavage", maxMissedCleavages+"");
		map.put("-minCharge", minCharge+"");
		map.put("-maxCharge", maxCharge+"");
		map.put("-expectedPeakWidth", expectedPeakWidth+"");
		map.put("-numberOfReportedPeaks", numberOfReportedPeaks+"");
		map.put("-addDecoysToBackground", addDecoysToBackgound+"");
		map.put("-dontRunDecoys", dontRunDecoys+"");
		map.put("-percolatorThreshold", percolatorThreshold+"");
		map.put("-percolatorVersion", percolatorVersionNumber+"");
		map.put("-percolatorVersionNumber", percolatorVersionNumber.getMajorVersion()+"");
		map.put(OPT_PERC_TRAINING_SIZE, Integer.toString(percolatorTrainingSetSize));
		map.put(OPT_PERC_TRAINING_THRESH, Float.toString(percolatorTrainingSetThreshold));
		map.put("-acquisition", DataAcquisitionType.toString(dataAcquisitionType));
		map.put("-numberOfThreadsUsed", numberOfThreadsUsed+"");
		map.put("-precursorWindowSize", precursorWindowSize+"");
		map.put("-numberOfQuantitativePeaks", numberOfQuantitativePeaks+"");
		map.put("-minNumOfQuantitativePeaks", minNumOfQuantitativePeaks+"");
		map.put("-alpha", alpha+"");
		map.put("-beta", beta+"");
		map.put("-quantifyAcrossSamples", quantifyAcrossSamples+"");
		map.put("-minIntensity", minIntensity+"");
		map.put("-requireVariableMods", "false");
        map.put("-precursorIsolationRangeFile", (precursorIsolationRangeFile.isPresent()?precursorIsolationRangeFile.get().getAbsolutePath():"none"));
        map.put("-percolatorModelFile", (percolatorModelFile.isPresent()?percolatorModelFile.get().getAbsolutePath():"none"));
		return map;
	}
	
	public void savePreferences(File backgroundFastaFile, File targetFastaFile) throws IOException,BackingStoreException {
		Preferences prefs=Preferences.userRoot().node("pecan");
		HashMap<String, String> map=toParameterMap();
		if (backgroundFastaFile!=null) map.put(Pecanpie.BACKGROUND_FASTA_TAG, backgroundFastaFile.getAbsolutePath());
		if (targetFastaFile!=null) map.put(Pecanpie.TARGET_FASTA_TAG, targetFastaFile.getAbsolutePath());
		for (Entry<String, String> entry : map.entrySet()) {
			//System.out.println("Writing Pecan preference "+entry.getKey()+" = "+entry.getValue());
			prefs.put(entry.getKey(), entry.getValue());
		}
		prefs.flush();
	}
	
	public static HashMap<String, String> readPreferences() throws IOException,BackingStoreException {
		Preferences prefs=Preferences.userRoot().node("pecan");
		HashMap<String, String> map=new HashMap<String, String>();
		for (String key : prefs.keys()) {
			String value=prefs.get(key, "");
			//System.out.println("Reading Pecan preference "+key+" = "+value);
			map.put(key, value);
		}
		return map;
	}
	
	/** used by CLI
	 */
	public PecanSearchParameters(
			AminoAcidConstants aaConstants, 
			FragmentationType fragType, 
			MassTolerance precursorTolerance, 
			double precursorOffsetPPM, 
			double precursorIsolationMargin, 
			MassTolerance fragmentTolerance,
			double fragmentOffsetPPM, 
			DigestionEnzyme enzyme, 
			float expectedPeakWidth,
			int minPeptideLength, 
			int maxPeptideLength, 
			int maxMissedCleavages, 
			byte minCharge, 
			byte maxCharge, 
			int numberOfReportedPeaks,
			boolean addDecoysToBackgound,
			boolean dontRunDecoys,
			float percolatorThreshold,
			float percolatorProteinThreshold,
			PercolatorVersion percolatorVersionNumber,
			int percolatorTrainingSetSize,
			float percolatorTrainingSetThreshold,
			float alpha,
			float beta,
			DataAcquisitionType dataAcquisitionType,
			int numberOfThreadsUsed,
			float targetWindowCenter,
			float precursorWindowSize, 
			int numberOfQuantitativePeaks, 
			int minNumOfQuantitativePeaks, 
			int topNTargetsUsed,
			float minIntensity, 
			boolean quantifyAcrossSamples, 
			boolean verifyModificationIons, 
			boolean requireVariableMods, 
			boolean filterPeaklists, 
			boolean doNotUseGlobalFDR, 
			Optional<File> precursorIsolationRangeFile, 
			Optional<File> percolatorModelFile, 
			boolean enableAdvancedOptions) {
		super(
				aaConstants,
				fragType,
				precursorTolerance,
				precursorOffsetPPM,
				precursorIsolationMargin,
				fragmentTolerance,
				fragmentOffsetPPM,
				fragmentTolerance,
				enzyme,
				percolatorThreshold,
				percolatorProteinThreshold,
				percolatorVersionNumber,
				percolatorTrainingSetSize,
				percolatorTrainingSetThreshold,
				dataAcquisitionType,
				numberOfThreadsUsed,
				expectedPeakWidth,
				targetWindowCenter,
				precursorWindowSize,
				numberOfQuantitativePeaks,
				minNumOfQuantitativePeaks,
				topNTargetsUsed,
				minIntensity,
				Optional.ofNullable((PeptideModification)null),
				ScoringBreadthType.ENTIRE_RT_WINDOW,
				0,
				quantifyAcrossSamples,
				verifyModificationIons,
				-1.0f,
				filterPeaklists,
				doNotUseGlobalFDR, 
				precursorIsolationRangeFile,
				percolatorModelFile,
				enableAdvancedOptions
		);
		this.minPeptideLength=minPeptideLength;
		this.maxPeptideLength=maxPeptideLength;
		this.maxMissedCleavages=maxMissedCleavages;
		this.minCharge=minCharge;
		this.maxCharge=maxCharge;
		this.numberOfReportedPeaks=numberOfReportedPeaks;
		this.addDecoysToBackgound=addDecoysToBackgound;
		this.dontRunDecoys=dontRunDecoys;
		this.alpha=alpha;
		this.beta=beta;
		this.requireVariableMods=requireVariableMods;
	}

	/** used by GUI
	 */
	public PecanSearchParameters(
			AminoAcidConstants aaConstants,
			FragmentationType fragType,
			MassTolerance precursorTolerance,
			MassTolerance fragmentTolerance,
			DigestionEnzyme enzyme,
			PercolatorVersion percolatorVersionNumber,
			float percolatorThreshold,
			float percolatorProteinThreshold,
			int percolatorTrainingSetSize,
			float percolatorTrainingSetThreshold,
			int maxMissedCleavages,
			byte minCharge,
			byte maxCharge,
			DataAcquisitionType dataAcquisitionType,
			float precursorWindowSize,
			int numberOfJobs,
			int numberOfQuantitativePeaks,
			int minNumOfQuantitativePeaks,
			int topNTargetsUsed,
			float minIntensity,
			float numberOfExtraDecoyLibrariesSearched,
			boolean quantifyAcrossSamples,
			boolean verifyModificationIons,
			boolean requireVariableMods
	) {
		super(
				aaConstants,
				fragType,
				precursorTolerance,
				0.0,
				0.0,
				fragmentTolerance,
				0.0,
				fragmentTolerance,
				enzyme,
				percolatorThreshold,
				percolatorProteinThreshold,
				percolatorVersionNumber,
				percolatorTrainingSetSize,
				percolatorTrainingSetThreshold,
				dataAcquisitionType,
				numberOfJobs,
				24f,
				-1f,
				precursorWindowSize,
				numberOfQuantitativePeaks,
				minNumOfQuantitativePeaks,
				topNTargetsUsed,
				minIntensity,
				Optional.ofNullable((PeptideModification)null),
				ScoringBreadthType.ENTIRE_RT_WINDOW,
				numberOfExtraDecoyLibrariesSearched,
				quantifyAcrossSamples,
				verifyModificationIons,
				-1.0f,
				false,
				false, 
				Optional.empty(),
				Optional.empty(),
				false
		);
		minPeptideLength=5;
		maxPeptideLength=100;
		this.maxMissedCleavages=maxMissedCleavages;
		this.minCharge=minCharge;
		this.maxCharge=maxCharge;
		numberOfReportedPeaks=1;
		addDecoysToBackgound=false;
		dontRunDecoys=false;
		alpha=1.8f;
		beta=0.4f;
		this.requireVariableMods=requireVariableMods;
	}

	public PecanSearchParameters(
			AminoAcidConstants aaConstants,
			FragmentationType fragType,
			MassTolerance fragmentTolerance,
			MassTolerance precursorTolerance,
			DigestionEnzyme enzyme,
			DataAcquisitionType dataAcquisitionType,
			boolean quantifyAcrossSamples,
			boolean verifyModificationIons,
			boolean requireVariableMods
	) {
		super(
				aaConstants,
				fragType,
				precursorTolerance,
				0.0,
				0.0,
				fragmentTolerance,
				0.0,
				fragmentTolerance,
				enzyme,
				0.01f,
				0.01f,
				null,
				PercolatorExecutor.DEFAULT_TRAINING_SET_SIZE,
				PercolatorExecutor.DEFAULT_TRAINING_THRESHOLD,
				dataAcquisitionType,
				Runtime.getRuntime().availableProcessors(),
				24f,
				-1f,
				-1f,
				5,
				3,
				-1,
				-1.0f,
				Optional.ofNullable((PeptideModification)null),
				ScoringBreadthType.ENTIRE_RT_WINDOW,
				0,
				quantifyAcrossSamples,
				verifyModificationIons,
				-1.0f,
				false,
				false,
				Optional.empty(),
				Optional.empty(),
				false);
		minPeptideLength=5;
		maxPeptideLength=100;
		maxMissedCleavages=1;
		minCharge=2;
		maxCharge=3;
		numberOfReportedPeaks=1;
		addDecoysToBackgound=false;
		dontRunDecoys=false;
		alpha=1.8f;
		beta=0.4f;
		this.requireVariableMods=requireVariableMods;
	}

	/**
	 * Used only for testing.
	 */
	public PecanSearchParameters(
			AminoAcidConstants aaConstants,
			FragmentationType fragType,
			MassTolerance fragmentTolerance,
			MassTolerance precursorTolerance,
			DigestionEnzyme enzyme,
			boolean quantifyAcrossSamples,
			boolean verifyModificationIons,
			boolean requireVariableMods
	) {
		super(
				aaConstants,
				fragType,
				precursorTolerance,
				0.0,
				0.0,
				fragmentTolerance,
				0.0,
				fragmentTolerance,
				enzyme,
				0.01f,
				0.01f,
				null,
				PercolatorExecutor.DEFAULT_TRAINING_SET_SIZE,
				PercolatorExecutor.DEFAULT_TRAINING_THRESHOLD,
				DataAcquisitionType.DIA,
				Runtime.getRuntime().availableProcessors(),
				24f,
				-1f,
				-1f,
				5,
				3,
				-1,
				-1.0f,
				Optional.ofNullable((PeptideModification)null),
				ScoringBreadthType.ENTIRE_RT_WINDOW,
				0,
				quantifyAcrossSamples,
				verifyModificationIons,
				-1.0f,
				false,
				false, 
				Optional.empty(),
				Optional.empty(),
				false
		);
		minPeptideLength=5;
		maxPeptideLength=100;
		maxMissedCleavages=1;
		minCharge=2;
		maxCharge=3;
		numberOfReportedPeaks=1;
		addDecoysToBackgound=false;
		dontRunDecoys=false;
		alpha=1.8f;
		beta=0.4f;
		this.requireVariableMods=requireVariableMods;
	}

	/**
	 * Used only for testing.
	 */
	public PecanSearchParameters(
			AminoAcidConstants aaConstants,
			FragmentationType fragType,
			MassTolerance fragmentTolerance,
			MassTolerance precursorTolerance,
			DigestionEnzyme enzyme,
			int maxMissedCleavages,
			boolean quantifyAcrossSamples,
			boolean verifyModificationIons,
			boolean requireVariableMods
	) {
		super(
				aaConstants,
				fragType,
				precursorTolerance,
				0.0,
				0.0,
				fragmentTolerance,
				0.0,
				fragmentTolerance,
				enzyme,
				0.01f,
				0.01f,
				null,
				PercolatorExecutor.DEFAULT_TRAINING_SET_SIZE,
				PercolatorExecutor.DEFAULT_TRAINING_THRESHOLD,
				DataAcquisitionType.DIA,
				Runtime.getRuntime().availableProcessors(),
				24f,
				-1f,
				-1f,
				5,
				3,
				-1,
				-1.0f,
				Optional.ofNullable((PeptideModification)null),
				ScoringBreadthType.ENTIRE_RT_WINDOW,
				0,
				quantifyAcrossSamples,
				verifyModificationIons,
				-1.0f,
				false,
				false, 
				Optional.empty(),
				Optional.empty(),
				false);
		this.maxMissedCleavages=maxMissedCleavages;
		minPeptideLength=5;
		maxPeptideLength=100;
		minCharge=2;
		maxCharge=3;
		numberOfReportedPeaks=1;
		addDecoysToBackgound=false;
		dontRunDecoys=false;
		alpha=1.8f;
		beta=0.4f;
		this.requireVariableMods=requireVariableMods;
	}

	public int getMaxMissedCleavages() {
		return maxMissedCleavages;
	}

	public int getMaxPeptideLength() {
		return maxPeptideLength;
	}

	public int getMinPeptideLength() {
		return minPeptideLength;
	}

	public byte getMaxCharge() {
		return maxCharge;
	}

	public byte getMinCharge() {
		return minCharge;
	}

	public int getNumberOfReportedPeaks() {
		return numberOfReportedPeaks;
	}

	public boolean isAddDecoysToBackgound() {
		return addDecoysToBackgound;
	}
	
	public boolean isDontRunDecoys() {
		return dontRunDecoys;
	}
	
	public float getAlpha() {
		return alpha;
	}
	
	public float getBeta() {
		return beta;
	}
	public boolean isRequireVariableMods() {
		return requireVariableMods;
	}
}

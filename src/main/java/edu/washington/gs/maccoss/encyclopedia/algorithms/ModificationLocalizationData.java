package edu.washington.gs.maccoss.encyclopedia.algorithms;

import edu.washington.gs.maccoss.encyclopedia.algorithms.phospho.AmbiguousPeptideModSeq;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.FragmentIon;

public class ModificationLocalizationData {
	public static final ModificationLocalizationData POISON_RESULT=new ModificationLocalizationData();
	
	private final AmbiguousPeptideModSeq localizationPeptideModSeq;
	private final float retentionTimeApexInSeconds;
	private final float localizationScore;
	private final float numIdentificationPeaks;
	private final int numberOfMods;
	private final boolean isSiteSpecific;
	private final boolean isLocalized;
	private final FragmentIon[] localizingIons;
	private final float localizingIntensity;
	private final float totalIntensity;

	private ModificationLocalizationData() {
		this.localizationPeptideModSeq=null;
		this.retentionTimeApexInSeconds=0.0f;
		this.localizationScore=0.0f;
		this.numIdentificationPeaks=0.0f;
		this.numberOfMods=0;
		this.isSiteSpecific=false;
		this.isLocalized=false;
		this.localizingIons=null;
		this.localizingIntensity=0.0f;
		this.totalIntensity=0.0f;
	}
	
	public ModificationLocalizationData(AmbiguousPeptideModSeq localizationPeptideModSeq, float retentionTimeApexInSeconds, float localizationScore, float numIdentificationPeaks, int numberOfMods, boolean isSiteSpecific, boolean isLocalized, boolean isCompletelyAmbiguous,
			FragmentIon[] localizingIons, float localizingIntensity, float totalIntensity) {
		this.localizationPeptideModSeq=localizationPeptideModSeq;
		this.retentionTimeApexInSeconds=retentionTimeApexInSeconds;
		this.localizationScore=localizationScore;
		this.numIdentificationPeaks=numIdentificationPeaks;
		this.numberOfMods=numberOfMods;
		if (this.localizationPeptideModSeq!=null&&this.localizationPeptideModSeq.getNumModifiableSites()==numberOfMods) {
			this.isSiteSpecific=true;
			this.isLocalized=true;
		} else if (isCompletelyAmbiguous||localizingIons.length==0) {
			this.isSiteSpecific=false;
			this.isLocalized=false;
		} else {
			this.isSiteSpecific=isSiteSpecific;
			this.isLocalized=isLocalized;
		}
		this.localizingIons=localizingIons;
		this.localizingIntensity=localizingIntensity;
		this.totalIntensity=totalIntensity;
	}

	public float getRetentionTimeApexInSeconds() {
		return retentionTimeApexInSeconds;
	}

	public AmbiguousPeptideModSeq getLocalizationPeptideModSeq() {
		return localizationPeptideModSeq;
	}

	public float getLocalizationScore() {
		return localizationScore;
	}
	
	public float getNumIdentificationPeaks() {
		return numIdentificationPeaks;
	}

	public int getNumberOfMods() {
		return numberOfMods;
	}

	public boolean isSiteSpecific() {
		return isSiteSpecific;
	}
	
	public boolean isLocalized() {
		return isLocalized;
	}

	public FragmentIon[] getLocalizingIons() {
		return localizingIons;
	}

	public float getLocalizingIntensity() {
		return localizingIntensity;
	}

	public float getTotalIntensity() {
		return totalIntensity;
	}
}

package edu.washington.gs.maccoss.encyclopedia.utils;

/**
 * also see Optional
 * @author searleb
 *
 */
public class Nothing {
	public static final Nothing NOTHING=new Nothing();

	private Nothing() {
	}
	
}

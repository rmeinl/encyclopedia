package edu.washington.gs.maccoss.encyclopedia.datastructures;

public interface HasRetentionTime {

	float getRetentionTimeInSec();

}
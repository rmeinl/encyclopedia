package edu.washington.gs.maccoss.encyclopedia.utils.math;

public class QuickMedianDouble {
	public static double median(double[] data) {
		return select(data, 0.5f);
	}
	
	public static double iqr(double[] data) {
		return select(data, 0.75f)-select(data, 0.25f);
	}

	/**
	 * this is destructive to the array order! (it sorts it)
	 * @param data
	 * @param desiredPercentage
	 * @return
	 */
	public static double select(double[] data, double desiredPercentage) {
		if (data.length==0) return 0.0f;

		// The exact position of the value we want to select. Note that this might fall between two values.
		final double pos = (data.length - 1) * desiredPercentage;

		// The fractional part of the position gives the proportion we should use to combine values if we fall between two.
		final double prop = pos % 1;

		// The desired target -- one more than the integral part of the position.
		int targetIndex=(int)Math.round(pos - prop) + 1;

		int left=0;
		int right=data.length-1;

		int seed=RandomGenerator.randomInt(data.length);

		while (true) {
			seed=RandomGenerator.randomInt(seed);
			if (left==right) {
				if (left < data.length - 1) {
					// guaranteed to be locally sorted because this point will always be sandwiched between two pivot points
					return ((1 - prop) * data[left]) + (prop * data[left + 1]);
				} else {
					return data[left];
				}
			}
			int pivotIndex=left+(int)Math.round((right-left)*RandomGenerator.floatFromRandomInt(seed));
			if (pivotIndex<0) {
				pivotIndex=0;
			}
			int pivotNewIndex=partition(data, left, right, pivotIndex);

			int pivotDist=pivotNewIndex-left+1;
			if (pivotDist==targetIndex) {
				if (pivotNewIndex<(data.length-1)) {
					double lowest=data[pivotNewIndex+1];
					for (int i=pivotNewIndex+2; i<=right; i++) {
						if (data[i]<lowest) {
							lowest=data[i];
						}
					}
					return ((1 - prop) * data[pivotNewIndex]) + (prop * lowest);
				} else {
					return data[pivotNewIndex];
				}
			} else if (targetIndex<pivotDist) {
				// the target is left of the pivot
				if (pivotNewIndex < 1) {
					// There's no data left of the pivot -- this means left is zero (because pivotNewIndex >= left always).
					// We just set right to zero and let the loop terminate next time around (because left==right).
					right = 0;
				} else {
					right = pivotNewIndex - 1;
				}
			} else {
				// the target is right of the pivot
				targetIndex=targetIndex-pivotDist;
				left=pivotNewIndex+1;
			}
		}
	}

	private static int partition(double[] data, int left, int right, int pivotIndex) {
		double pivotValue=data[pivotIndex];
		swap(data, pivotIndex, right);
		int storeIndex=left;
		for (int i=left; i<=right; i++) {
			if (data[i]<pivotValue) {
				swap(data, storeIndex, i);
				storeIndex++;
			}
		}
		swap(data, right, storeIndex);
		return storeIndex;
	}

	/**
	 * Method to swap to elements in an array.
	 * 
	 * @param a
	 *            an array of objects.
	 * @param index1
	 *            the index of the first object.
	 * @param index2
	 *            the index of the second object.
	 */
	public static final void swap(double[] a, int index1, int index2) {
		double tmp=a[index1];
		a[index1]=a[index2];
		a[index2]=tmp;
	}
}

EncyclopeDIA

### Contribution guidelines ###
Any contribution must follow the coding style of the project, be presented with tests and stand up to code review before it will be accepted.

### Who do I talk to? ###
This is a [Searle Lab](http://www.searlelab.org/) and [MacCoss Lab](https://www.maccosslab.org/) project from the [Department of Biomedical Informatics](https://medicine.osu.edu/departments/biomedical-informatics) at the Ohio State University and the University of Washington, [Department of Genome Sciences](http://www.gs.washington.edu/). For more information please contact [Brian Searle](http://www.searlelab.org/people/brian_searle/index.html) (brian dot searle at osumc dot edu).